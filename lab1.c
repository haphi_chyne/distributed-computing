
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h> 
#include <sys/types.h> 
#include <sys/wait.h>
#define Max 20

int main(int argc, char *argv[]) {
    int fd1[2], fd2[2], fd3[2];
    int index, element, size = 6;
    int array[] = {1,2,3,8,4,5}; 

    // create pipe descriptors
    pipe(fd1);
    pipe(fd2);
    pipe(fd3);
    
    int X = fork();

    // fork() returns 0 for child process, child-pid for parent process.
    if ( X != 0) {

        // send the element on the write-descriptor.
	    for ( int i = 0;i < size; i++)
        {
            printf(" %d ",array[i]);
        }
	    printf("\n Enter index: ");
        scanf("%d", &index);

        write(fd1[1], &array, sizeof(array));
        write(fd2[1], &index, sizeof(index));

        // close the write descriptor
        close(fd1[1]);
        close(fd2[1]);
        
        read(fd3[0], &element, sizeof(element));
        // close the read-descriptor
        close(fd3[0]);
        
        printf("Element %d is found at index %d \n", element, index);
      
    } else if(X==0){
        
        int arr[Max];
        int in;
        
        // now read the data (will block until it succeeds)
        read(fd1[0], &arr, sizeof(arr));
        read(fd2[0], &in, sizeof(in));
        
        // close the read-descriptor
        close(fd1[0]);
        close(fd2[0]);
        int el = arr[in];
       
	    write(fd3[1], &el, sizeof(el));
        // close the write descriptor
        close(fd3[1]);
    }
    return 0;
}


