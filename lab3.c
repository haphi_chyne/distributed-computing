// Suppose there exist a file and you have to red, write and update the file concurrently.
// Write a multi threaded program such that there should be different threads for all the
// different task and each thread access the file synchronously.

#include <pthread.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <unistd.h> 
#define N 10

pthread_t rtid[N];  
pthread_t wtid[N];  
pthread_mutex_t mutex;    /* controls access to rc */
pthread_mutex_t  db;       /* controls access to the database */
int rc = 0;             /* # of processes reading or wanting to */
int Rd = N;
int Wr = N;

void *reader(void)
{
        pthread_mutex_lock(&mutex);           /* get exclusive access to rc */
        rc = rc + 1;            /* one reader more now */
        if (rc == 1) 
            pthread_mutex_lock(&db); /* if this is the first reader ... */
        pthread_mutex_unlock(&mutex);             /* release exclusive access to rc */
        
        read_data_base( );      /* access the data */
        
        pthread_mutex_lock(&mutex);           /* get exclusive access to rc */
        rc = rc - 1;            /* one reader few er now */
        if (rc == 0)
            pthread_mutex_unlock(&db);   /* if this is the last reader ... */
        pthread_mutex_unlock(&mutex);             /* release exclusive access to rc */
} 

void *writer(void)
{
        pthread_mutex_lock(&db);              /* get exclusive access */
        write_data_base( );    /* update the data */
        pthread_mutex_unlock(&db);                /* release exclusive access */
} 

read_data_base( )
{
    printf("\nReading from database");
    FILE *fptr;
    char c[1000];
    if ((fptr = fopen("/home/mosix/DistributedComputing Lab/file.txt", "r")) == NULL) {
        printf("Error! opening file");
        exit(1);
    }

    // reads text until newline is encountered
    fscanf(fptr, "%[^\n]", c);
    printf("Data from the file:\n%s", c);
    fclose(fptr);
}

write_data_base( ){
    printf("\nWriting to database");
    FILE * fp;
   int i;
   /* open the file for writing*/
   fp = fopen ("/home/mosix/DistributedComputing Lab/file.txt","w");
 
   /* write 10 lines of text into the file stream*/
   for(i = 0; i < 10;i++){
       fprintf (fp, "\nWriting file");
   }
 
   /* close the file*/  
   fclose (fp);
}


void main()
{
    pthread_mutex_init(&mutex, NULL);
    pthread_mutex_init(&db, NULL);

    for(int i= 0; i<N; i++)
    {
        pthread_create(&rtid[i],NULL, reader, NULL);
        pthread_create(&wtid[i],NULL, writer, NULL);
    }

    for(int i= 0; i<N; i++)
    {
        pthread_join(rtid[i],NULL);
        pthread_join(wtid[i],NULL);
    }

}
