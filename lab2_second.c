
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h> 
#include <sys/types.h> 
#include <sys/wait.h>

void main()
{
    pid_t p =  fork();

    if(p>0)
    {
        printf("P0: \n");
    }
    pid_t p1 = getppid();
    pid_t p2 = getpid();
 
    printf("parent id= %d\t child id= %d \n", p1, p2);
    p= fork();
    if(p>0)
    {
        printf("P1: \n");
    }
    p1 = getppid();
    p2 = getpid();

    printf("parent id= %d\t child id= %d \n", p1, p2);
    p= fork();
    if(p>0)
    {
        printf("P2: \n");
    }
    p1 = getppid();
    p2 = getpid();

    printf("parent id= %d\t child id= %d \n", p1, p2);

}