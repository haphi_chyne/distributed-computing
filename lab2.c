// WAP in which the parent process sends 2 matrices to its child process 
// through a pipe and a child process returns the sum of the matrices to 
// the parent through a pipe. The parent should print the result.

// Roll Number: B16CS007

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h> 
#include <sys/types.h> 
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    int fd[2];

    // create pipe descriptor
    pipe(fd);
    
    pid_t pid = fork();

    // fork() returns 0 for child process, child-pid for parent process.
    if ( pid > 0) {

        int mat1[3][3], mat2[3][3], mat3[3][3];

        for(int i=0; i<3; i++)
        {
            for(int j=0; j<3; j++)
            {
                mat1[i][j] = j;
                mat2[j][i] = j;
            }
        }
            
        // send the matrices on the write-descriptor.
        write(fd[1], &mat1, sizeof(mat1));
        write(fd[1], &mat2, sizeof(mat2));
        
        wait(NULL);
        //read sum of matrices from child
        read(fd[0], &mat3, sizeof(mat3));
        
        printf("Matrix 1 : \t\t Matrix 2 :\n\n");
        for(int i=0; i<3; i++, printf("\n"))
        {
            for(int j=0; j<3; j++)
                printf(" %d ", mat1[i][j]);
            printf("\t\t ");
            for(int k=0; k<3; k++)
                printf(" %d ", mat2[i][k]);
        }

        printf("\nMatrix 1 + Matrix 2 : \n\n");
        for(int i=0; i<3; i++, printf("\n"))
        {
            for(int j=0; j<3; j++)
            {
                printf(" %d ", mat3[i][j]);
            }
        }
      
    } else if(pid==0){
        
        int mat1[3][3], mat2[3][3], mat3[3][3];
        
        // read matrices from parent process
        read(fd[0], &mat1, sizeof(mat1));
        read(fd[0], &mat2, sizeof(mat2));

         for(int i=0; i<3; i++, printf("\n"))
        {
            for(int j=0; j<3; j++)
            {
                mat3[i][j] = mat1[i][j] + mat2[i][j];
            }
        }       

        // Send the sum of matrices to parent process
	    write(fd[1], &mat3, sizeof(mat3));
    }
    return 0;
}


